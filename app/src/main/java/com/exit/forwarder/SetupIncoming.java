package com.exit.forwarder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class SetupIncoming extends Activity {
    public final static String EXTRA_MESSAGE = "com.exit.forwarder.NUMBER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_incoming);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.setup_incoming, menu);
        //getMenuInflater().inflate(R.menu.activity_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addNumber(View view){
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText numInputBox = (EditText)findViewById(R.id.edit_message);
        String num = numInputBox.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, num);
        startActivity(intent);
    }
}
