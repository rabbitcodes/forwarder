package com.exit.forwarder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by grajagopalan on 3/3/15.
 */
public class Receiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String str = "";
        SmsMessage msgs[] = null;
        if(bundle!=null){
            Object[] pdus = (Object[])bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];
            for (int i=0; i<msgs.length;i++){
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                str += "SMS from " + msgs[i].getOriginatingAddress()+ " :" + msgs[i].getMessageBody().toString();
            }

        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();

            SmsManager mgr = SmsManager.getDefault();
            mgr.sendTextMessage("5556",null,"Ack",null,null);

        }
    }
}
